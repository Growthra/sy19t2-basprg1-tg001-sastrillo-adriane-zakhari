#include <iostream>
#include <time.h>

using namespace std;

int main()
{
	
	// Intro
	bool start;
	cout << "WELCOME TO THE GAME OF MAGIK" << endl;
	cout << "THE RULES ARE THAT" << endl;
	cout << endl;
	cout << "Move  " << "Beat  " << "Lose  " << endl;
	cout << " ||  " << "  ||  " << "  ||  " << endl;
	cout << "Spell  " << "Barrier  " << "Special  " << endl;
	cout << "Barrier  " << "Special  " << "Spell  " << endl;
	cout << "Special  " << " Spell  " << "Berrier  " << endl;
	cout << endl;
	cout << "TO START PRESS 1 & ENTER" << endl;
	cout << "TO QUIT PRESS 0 & ENTER" << endl;
	cin >> start;

	if (start == 0)
	{
		return 0;
	}
	else
	{
		system("cls");
	}

	//VARIABLES
	int hp, dmg, move, minDmg, maxDmg, aiMove, aiDmg, aiHp, aiMinDmg, aiMaxDmg;

	//Set up
	cout << "Game Set up:" << endl;
	cout << endl;
	cout << "What's your Hp?" << endl;
	cin >> hp;
	cout << endl;
	cout << "What's your Minimum Damage?" << endl;
	cin >> minDmg;
	cout << endl;
	cout << "What's your Max Damage?" << endl;
	cin >> maxDmg;

	system("cls");

	cout << "What's your Enmey's Hp?" << endl;
	cin >> aiHp;
	cout << endl;
	cout << "What's your Enmey's Minimum Damage?" << endl;
	cin >> aiMinDmg;
	cout << endl;
	cout << "What's your Enmey's Max Damage?" << endl;
	cin >> aiMaxDmg;
	cout << endl;

	if (minDmg > maxDmg)
	{
		cout << "Your Minimum Damage can't excced the Max Damage";
		return 0;
	}
	else if (aiMinDmg > aiMaxDmg) {

		cout << "The Enemy's Minimum Damage can't excced the Max Damage";
		return 0;
	}
	system("CLS");

	//Winning Conditions
	bool condition1 = aiHp <= 0;
	bool condition2 = hp <= 0;
	bool condition3 = aiHp <= 0 && hp <= 0;

	

		cout << "Your HP: " << hp << endl;
		cout << "Enemy HP: " << aiHp << endl;
		cout << endl;
		cout << "|Pick Your Action:" << endl;
		cout << "|-Spell = 1" << endl;
		cout << "|-Barrier = 2" << endl;
		cout << "|-Special = 3" << endl;
		cin >> move;
	

	system("cls");

	//The Game
	while (!(condition1) || (condition2) || (condition3))
	{
		// More Var
		srand(time(0));
		dmg = rand() % (maxDmg - minDmg + 1) + minDmg;
		aiDmg = rand() % (aiMaxDmg - aiMinDmg + 1) + aiMinDmg;
		aiMove = rand() % 3 + 1;

		//Spells
		int spell = aiHp - dmg;
		int aiSpell = hp - aiDmg;

		//Barrier
		float barrier = aiHp - (dmg / 0.5f);
		float aiBarrier = hp - (aiDmg / 0.5f);

		//Special
		int special = aiHp - (dmg * 2);
		int aiSpecial = hp - (aiDmg * 2);

		// Hp regulation

		if (hp < 0) hp = 0;
		if (aiHp < 0) aiHp = 0;

		

		//Spell
		if (move == 1) {
			if (aiMove == 1) {
				cout << "both of attacked so both get damaged" << endl;
				cout << endl;
				cout << "Your HP: " << hp << endl;
				cout << "Enemy HP: " << aiHp << endl;
				cout << endl;
				cout << "|Pick Your Action:" << endl;
				cout << "|-Spell = 1" << endl;
				cout << "|-Barrier = 2" << endl;
				cout << "|-Special = 3" << endl;
				aiHp = spell;
				hp = aiSpell;
				cin >> move;
			}
			else if (aiMove == 2) {
				cout << "the enemy defended the attack. Enemy get halfed damaged" << endl;
				cout << endl;
				cout << "Your HP: " << hp << endl;
				cout << "Enemy HP: " << aiHp << endl;
				cout << endl;
				cout << "|Pick Your Action:" << endl;
				cout << "|-Spell = 1" << endl;
				cout << "|-Barrier = 2" << endl;
				cout << "|-Special = 3" << endl;
				aiHp = barrier;
				cin >> move;
			}
			else if (aiMove == 3) {
				cout << "The enemy used special so you get more damaged but still attacked" << endl;
				cout << endl;
				cout << "Your HP: " << hp << endl;
				cout << "Enemy HP: " << aiHp << endl;
				cout << endl;
				cout << "|Pick Your Action:" << endl;
				cout << "|-Spell = 1" << endl;
				cout << "|-Barrier = 2" << endl;
				cout << "|-Special = 3" << endl;
				aiHp = spell;
				hp = aiSpecial;
				cin >> move;
			}
			else {
				cout << "invalid";
			}
		}

		//Barrier
		else if (move == 2) {
			if (aiMove == 1) {
				cout << "you defended the attack. you get halfed damaged" << endl;
				cout << endl;
				cout << "Your HP: " << hp << endl;
				cout << "Enemy HP: " << aiHp << endl;
				cout << endl;
				cout << "|Pick Your Action:" << endl;
				cout << "|-Spell = 1" << endl;
				cout << "|-Barrier = 2" << endl;
				cout << "|-Special = 3" << endl;
				hp = aiBarrier;
				cin >> move;
			}
			else if (aiMove == 2) {
				cout << "both defended.... nothing happens" << endl;
				cout << endl;
				cout << "Your HP: " << hp << endl;
				cout << "Enemy HP: " << aiHp << endl;
				cout << endl;
				cout << "|Pick Your Action:" << endl;
				cout << "|-Spell = 1" << endl;
				cout << "|-Barrier = 2" << endl;
				cout << "|-Special = 3" << endl;
				cin >> move;
			}
			else if (aiMove == 3) {
				cout << "The enemy used special so you get more damaged and you take it like a real magik boi" << endl;
				cout << endl;
				cout << "Your HP: " << hp << endl;
				cout << "Enemy HP: " << aiHp << endl;
				cout << endl;
				cout << "|Pick Your Action:" << endl;
				cout << "|-Spell = 1" << endl;
				cout << "|-Barrier = 2" << endl;
				cout << "|-Special = 3" << endl;
				hp = aiSpecial;
				cin >> move;
			}
			else {
				cout << "invalid";
			}
		}

		//Special
		else if (move == 3) {
			if (aiMove == 1) {
				cout << "You used special so The Enemy get more damaged but The Enemy still attacked" << endl;
				cout << endl;
				cout << "Your HP: " << hp << endl;
				cout << "Enemy HP: " << aiHp << endl;
				cout << endl;
				cout << "|Pick Your Action:" << endl;
				cout << "|-Spell = 1" << endl;
				cout << "|-Barrier = 2" << endl;
				cout << "|-Special = 3" << endl;
				hp = aiSpell;
				aiHp = special;
				cin >> move;
			}
			else if (aiMove == 2) {
				cout << "You used special so The Enemy get more damaged and He takes it like a real magik boi" << endl;
				cout << endl;
				cout << "Your HP: " << hp << endl;
				cout << "Enemy HP: " << aiHp << endl;
				cout << endl;
				cout << "|Pick Your Action:" << endl;
				cout << "|-Spell = 1" << endl;
				cout << "|-Barrier = 2" << endl;
				cout << "|-Special = 3" << endl;
				aiHp = special;
				cin >> move;
			}
			else if (aiMove == 3) {
				cout << "You used special so The Enemy get more damaged and He takes it like a real magik boi" << endl;
				cout << endl;
				cout << "Your HP: " << hp << endl;
				cout << "Enemy HP: " << aiHp << endl;
				cout << endl;
				cout << "|Pick Your Action:" << endl;
				cout << "|-Spell = 1" << endl;
				cout << "|-Barrier = 2" << endl;
				cout << "|-Special = 3" << endl;
				aiHp = special;
				hp = aiSpecial;
				cin >> move;
			}
			else {
				cout << "invalid";
			}

			if (condition1) {

				cout << "YOU WON MAGE BATTLE" << endl;
				system("pause");
				return 0;
			}
			else if (condition2) {

				cout << "YOU LOST LIKE A PLEB" << endl;
				system("pause");
				return 0;
			}
			else if (condition3) {

				cout << "BOTH OF YOU AND THE ENEMY FIANTED" << endl;
				cout << "               DRAW              " << endl;
				system("pause");
				return 0;
			}

		}
		else {
		cout << "invalid";
		return 0;
		}

		system("cls");

	}
	
	
	system("pause");
	return 0;
}

	